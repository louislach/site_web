# DofusDB - Guide de contribution

## Installation
Suivez le guide dans le README.md

## Faire fonctionner l'api en local
Dans le fichier **quasar.conf.js**:<br/>
Remplacer la ligne:<br/>
```js
API_URL: (ctx.dev) ? 'http://localhost:3030' : 'https://api.dofusdb.fr',
```
Par:<br/>
```js
API_URL: 'https://api.dofusdb.fr',
```

## Documentation du framework
Quasar est utilisé sur ce projet.<br/>
Vous pouvez retrouver la documentation à l'adresse suivante: https://quasar.dev
