import axios from 'axios';

export function getSubArea(id, query) {
  return axios({
    method: 'GET',
    url: `/subareas/${id}`,
    params: query,
  });
}

export function getSubAreas(query) {
  return axios({
    method: 'GET',
    url: '/subareas',
    params: query,
  });
}
