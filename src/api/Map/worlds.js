import axios from 'axios';

export function getWorld(id, query) {
  return axios({
    method: 'GET',
    url: `/world/${id}`,
    params: query,
  });
}

export function getWorlds(query) {
  return axios({
    method: 'GET',
    url: '/worlds',
    params: query,
  });
}
