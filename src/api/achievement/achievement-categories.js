import axios from 'axios';

export function getAchievementCategories(query) {
  return axios({
    method: 'GET',
    url: '/achievement-categories',
    params: query,
  });
}

export function getAchievementCategory(id, query) {
  return axios({
    method: 'GET',
    url: `/achievement-categories/${id}`,
    params: query,
  });
}
