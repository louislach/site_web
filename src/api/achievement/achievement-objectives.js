import axios from 'axios';

export function getAchievementObjectives(query) {
  return axios({
    method: 'GET',
    url: '/achievement-objectives',
    params: query,
  });
}

export function getAchievementObjective(id, query) {
  return axios({
    method: 'GET',
    url: `/achievement-objectives/${id}`,
    params: query,
  });
}
