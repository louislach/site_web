import axios from 'axios';

export function getAchievementRewards(query) {
  return axios({
    method: 'GET',
    url: '/achievement-rewards',
    params: query,
  });
}

export function getAchievementReward(id, query) {
  return axios({
    method: 'GET',
    url: `/achievement-rewards/${id}`,
    params: query,
  });
}
