import axios from 'axios';

export function getAchievements(query) {
  return axios({
    method: 'GET',
    url: '/achievements',
    params: query,
  });
}

export function getAchievement(id, query) {
  return axios({
    method: 'GET',
    url: `/achievements/${id}`,
    params: query,
  });
}
