import axios from 'axios';

export function getEffect(id, query) {
  return axios({
    method: 'GET',
    url: `/effects/${id}`,
    params: query,
  });
}

export function getEffects(query) {
  return axios({
    method: 'GET',
    url: '/effects',
    params: query,
  });
}
