import axios from 'axios';

export function getGroupFinders(query) {
  return axios({
    method: 'GET',
    url: '/groupfinder',
    params: query,
  });
}

export function getGroupFinder(id, query) {
  return axios({
    method: 'GET',
    url: `/groupfinder/${id}`,
    params: query,
  });
}

export function createGroupFinder(data) {
  return axios({
    method: 'POST',
    url: '/groupfinder',
    data,
  });
}

export function patchGroupFinder(id, data) {
  return axios({
    method: 'PATCH',
    url: `/groupfinder/${id}`,
    data,
  });
}

export function deleteGroupFinder(id) {
  return axios({
    method: 'DELETE',
    url: `/groupfinder/${id}`,
  });
}
