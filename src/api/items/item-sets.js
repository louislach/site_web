import axios from 'axios';

export function getItemSet(id, query) {
  return axios({
    method: 'GET',
    url: `/item-sets/${id}`,
    params: query,
  });
}

export function getItemSets(query) {
  return axios({
    method: 'GET',
    url: '/item-sets',
    params: query,
  });
}
