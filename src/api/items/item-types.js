import axios from 'axios';

export function getItemTypes(query) {
  return axios({
    method: 'GET',
    url: '/item-types',
    params: query,
  });
}

export function getItemType(id, query) {
  return axios({
    method: 'GET',
    url: `/item-types/${id}`,
    params: query,
  });
}
