import axios from 'axios';

export function getItems(query) {
  return axios({
    method: 'GET',
    url: '/items',
    params: query,
  });
}

export function getItem(id, query) {
  return axios({
    method: 'GET',
    url: `/items/${id}`,
    params: query,
  });
}
