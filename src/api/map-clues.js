import axios from 'axios';

export function getMapClue(id, query) {
  return axios({
    method: 'GET',
    url: `/map-clue/${id}`,
    params: query,
  });
}

export function getMapClues(query) {
  return axios({
    method: 'GET',
    url: '/map-clue',
    params: query,
  });
}

export function createMapClue(data, query) {
  return axios({
    method: 'POST',
    url: '/map-clue',
    data,
    params: query,
  });
}
