import axios from 'axios';

export function getMonsters(query) {
  return axios({
    method: 'GET',
    url: '/monsters',
    params: query,
  });
}

export function getMonster(id, query) {
  return axios({
    method: 'GET',
    url: `/monsters/${id}`,
    params: query,
  });
}
