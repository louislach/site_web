import axios from 'axios';

export function getPointOfInterest(id, query) {
  return axios({
    method: 'GET',
    url: `/point-of-interest/${id}`,
    params: query,
  });
}

export function getPointOfInterests(query) {
  return axios({
    method: 'GET',
    url: '/point-of-interest',
    params: query,
  });
}
