import axios from 'axios';

export function getPortals(query) {
  return axios({
    method: 'GET',
    url: '/portals',
    params: query,
  });
}

export function getPortal(id, query) {
  return axios({
    method: 'GET',
    url: `/portals/${id}`,
    params: query,
  });
}

export function createPortal(data) {
  return axios({
    method: 'POST',
    url: '/portals',
    data,
  });
}

export function patchPortal(id, data) {
  return axios({
    method: 'PATCH',
    url: `/portals/${id}`,
    data,
  });
}

export function deletePortal(id) {
  return axios({
    method: 'DELETE',
    url: `/portals/${id}`,
  });
}
