import axios from 'axios';

export function getQuestCategories(query) {
  return axios({
    method: 'GET',
    url: '/quest-categories',
    params: query,
  });
}

export function getQuestCategory(id, query) {
  return axios({
    method: 'GET',
    url: `/quest-categories/${id}`,
    params: query,
  });
}
