import axios from 'axios';

export function getQuestObjectives(query) {
  return axios({
    method: 'GET',
    url: '/quest-objectives',
    params: query,
  });
}

export function getQuestObjective(id, query) {
  return axios({
    method: 'GET',
    url: `/quest-objectives/${id}`,
    params: query,
  });
}
