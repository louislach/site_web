import axios from 'axios';

export function getQuestStepRewards(query) {
  return axios({
    method: 'GET',
    url: '/quest-step-rewards',
    params: query,
  });
}

export function getQuestStepReward(id, query) {
  return axios({
    method: 'GET',
    url: `/quest-step-rewards/${id}`,
    params: query,
  });
}
