import axios from 'axios';

export function getQuestStep(id, query) {
  return axios({
    method: 'GET',
    url: `/quest-steps/${id}`,
    params: query,
  });
}

export function getQuestSteps(query) {
  return axios({
    method: 'GET',
    url: '/quest-steps',
    params: query,
  });
}
