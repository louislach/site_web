import axios from 'axios';

export function getQuests(query) {
  return axios({
    method: 'GET',
    url: '/quests',
    params: query,
  });
}

export function getQuest(id, query) {
  return axios({
    method: 'GET',
    url: `/quests/${id}`,
    params: query,
  });
}
