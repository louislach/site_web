import axios from 'axios';

export function getRecipes(query) {
  return axios({
    method: 'GET',
    url: '/recipes',
    params: query,
  });
}

export function getRecipe(id, query) {
  return axios({
    method: 'GET',
    url: `/recipes/${id}`,
    params: query,
  });
}
