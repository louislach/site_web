import axios from 'axios';

export function getReputationPoints(id, query) {
  return axios({
    method: 'GET',
    url: `/reputation-points/${id}`,
    params: query,
  });
}

export function getReputationPoint(query) {
  return axios({
    method: 'GET',
    url: '/reputation-points',
    params: query,
  });
}

export function createReputationPoint(data, query) {
  return axios({
    method: 'POST',
    url: '/reputation-points',
    data,
    params: query,
  });
}
