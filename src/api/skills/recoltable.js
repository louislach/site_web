import axios from 'axios';

export function getAllRecoltables(params, options) {
  return axios({
    method: 'GET',
    url: '/recoltable',
    params,
    ...options,
  });
}
