import axios from 'axios';

export function getSkill(id, params) {
  return axios({
    method: 'GET',
    url: `/skills/${id}`,
    params,
  });
}

export function getAllSkills(params) {
  return axios({
    method: 'GET',
    url: '/skills',
    params,
  });
}
