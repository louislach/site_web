import axios from 'axios';

export function getSpellLevel(id, query) {
  return axios({
    method: 'GET',
    url: `/spell-levels/${id}`,
    params: query,
  });
}

export function getSpellLevels(query) {
  return axios({
    method: 'GET',
    url: '/spell-levels',
    params: query,
  });
}
