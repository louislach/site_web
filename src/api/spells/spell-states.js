import axios from 'axios';

export function getSpellState(id, query) {
  return axios({
    method: 'GET',
    url: `/spell-states/${id}`,
    params: query,
  });
}

export function getSpellStates(query) {
  return axios({
    method: 'GET',
    url: '/spell-states',
    params: query,
  });
}
