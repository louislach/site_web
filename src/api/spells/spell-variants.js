import axios from 'axios';

export function getSpellVariant(id, query) {
  return axios({
    method: 'GET',
    url: `/spell-variants/${id}`,
    params: query,
  });
}

export function getSpellVariants(query) {
  return axios({
    method: 'GET',
    url: '/spell-variants',
    params: query,
  });
}
