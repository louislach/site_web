import axios from 'axios';

export function getSpell(id, query) {
  return axios({
    method: 'GET',
    url: `/spells/${id}`,
    params: query,
  });
}

export function getSpells(query) {
  return axios({
    method: 'GET',
    url: '/spells',
    params: query,
  });
}
