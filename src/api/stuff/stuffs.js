import axios from 'axios';

export function getStuffs(query) {
  return axios({
    method: 'GET',
    url: '/stuffs',
    params: query,
  });
}

export function getStuff(id, query) {
  return axios({
    method: 'GET',
    url: `/stuffs/${id}`,
    params: query,
  });
}

export function createStuff(data) {
  return axios({
    method: 'POST',
    url: '/stuffs',
    data,
  });
}

export function putStuff(id, data) {
  return axios({
    method: 'PUT',
    url: `/stuffs/${id}`,
    data,
  });
}
