import axios from 'axios';

export function getCharacter(id, query) {
  return axios({
    method: 'GET',
    url: `/characters/${id}`,
    params: query,
  });
}

export function getCharacters(query) {
  return axios({
    method: 'GET',
    url: '/characters',
    params: query,
  });
}

export function createCharacter(body, query) {
  return axios({
    method: 'POST',
    url: '/characters',
    params: query,
    data: body,
  });
}

export function patchCharacter(id, body, query) {
  return axios({
    method: 'PATCH',
    url: `/characters/${id}`,
    params: query,
    data: body,
  });
}

export function deleteCharacter(id, query) {
  return axios({
    method: 'DELETE',
    url: `/characters/${id}`,
    params: query,
  });
}
