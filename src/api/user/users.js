import axios from 'axios';

export function getUsers(query) {
  return axios({
    method: 'GET',
    url: '/users',
    params: query,
  });
}

export function getUser(id, query) {
  return axios({
    method: 'GET',
    url: `/users/${id}`,
    params: query,
  });
}

export function createUser(data) {
  return axios({
    method: 'POST',
    url: '/users',
    data,
  });
}

export function patchUser(id, data) {
  return axios({
    method: 'PATCH',
    url: `/users/${id}`,
    data,
  });
}

export function deleteUser(id) {
  return axios({
    method: 'DELETE',
    url: `/users/${id}`,
  });
}
