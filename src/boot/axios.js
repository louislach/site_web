import Vue from 'vue';
import axios from 'axios';
import characteristics from 'src/enums/characteristics';
import * as Quests from 'src/api/quest/quests';
import * as QuestSteps from 'src/api/quest/quest-steps';
import * as QuestStepRewards from 'src/api/quest/quest-step-rewards';
import * as QuestCategories from 'src/api/quest/quest-categories';
import * as QuestObjectives from 'src/api/quest/quest-objectives';
import * as PointOfInterests from 'src/api/point-of-interests';
import * as MapClues from 'src/api/map-clues';
import * as ReputationPoints from 'src/api/reputation-points';
import * as Subarea from 'src/api/Map/subareas';
import * as Worlds from 'src/api/Map/worlds';
import * as Achievements from 'src/api/achievement/achievements';
import * as AchievementCategories from 'src/api/achievement/achievement-categories';
import * as AchievementRewards from 'src/api/achievement/achievement-rewards';
import * as AchievementObjectives from 'src/api/achievement/achievement-objectives';
import * as Character from 'src/api/user/character';
import * as SpellLevels from 'src/api/spells/spell-levels';
import * as SpellVariants from 'src/api/spells/spell-variants';
import * as SpellStates from 'src/api/spells/spell-states';
import * as Spells from 'src/api/spells/spells';
import * as Effects from 'src/api/effect/effect';
import * as Items from 'src/api/items/items';
import * as ItemSets from 'src/api/items/item-sets';
import * as ItemTypes from 'src/api/items/item-types';
import * as Monsters from 'src/api/monster/monsters';
import * as Stuffs from 'src/api/stuff/stuffs';
import * as GroupFinder from 'src/api/group-finder/group-finder';
import * as Portals from 'src/api/portal/portals';
import * as Recipes from 'src/api/recipe/recipes';
import * as Users from 'src/api/user/users';
import * as Skills from 'src/api/skills/skills';
import * as Recoltable from 'src/api/skills/recoltable';

Vue.prototype.$axios = axios;

let save = null;

export default ({ store, router }) => {
  if (store) {
    save = store;
  }

  axios.defaults.baseURL = process.env.API_URL;

  axios.interceptors.request.use((config) => {
    const { token } = save.state.user;

    if (token) {
      config.headers.Authorization = `Bearer ${token}`;
    }
    return config;
  }, (err) => Promise.reject(err));

  axios.interceptors.response.use((response) => response,
    (err) => {
      if (err?.response?.status === 401) {
        router.push({ name: 'signin' }).catch(() => {});
      } else {
        throw err;
      }
    });

  const api = {
    ...PointOfInterests,
    ...MapClues,
    ...ReputationPoints,
    ...Subarea,
    ...Achievements,
    ...AchievementCategories,
    ...AchievementObjectives,
    ...AchievementRewards,
    ...Character,
    ...Spells,
    ...SpellLevels,
    ...SpellVariants,
    ...SpellStates,
    ...Effects,
    ...Items,
    ...ItemSets,
    ...ItemTypes,
    ...Quests,
    ...QuestCategories,
    ...QuestSteps,
    ...QuestStepRewards,
    ...QuestObjectives,
    ...Monsters,
    ...Stuffs,
    ...GroupFinder,
    ...Portals,
    ...Recipes,
    ...Users,
    ...Skills,
    ...Recoltable,
    ...Worlds,
    isLogged() {
      return !!save.state.user.token;
    },
    async getAll(call, query) {
      let data = [];
      let response;
      do {
        // eslint-disable-next-line
        response = (await call({
          $skip: data.length,
          ...query,
        })).data;

        data = data.concat(response.data);
      } while (data.length < response.total);

      return data;
    },
    login(username, password) {
      return axios({
        method: 'POST',
        url: `${process.env.API_URL}/authentication`,
        data: {
          strategy: 'local',
          username,
          password,
        },
      });
    },
    getServers(query) {
      return axios({
        method: 'GET',
        url: `${process.env.API_URL}/servers`,
        params: query,
      });
    },
    getReadableCriteria(criteria) {
      return axios({
        method: 'GET',
        url: `${process.env.API_URL}/criterion/${criteria}`,
      });
    },
    getAllCharacteristics(query) {
      return axios({
        method: 'GET',
        url: `${process.env.API_URL}/characteristics`,
        params: query,
      });
    },
    getDimensions(query) {
      return axios({
        method: 'GET',
        url: `${process.env.API_URL}/dimensions`,
        params: query,
      });
    },
    getBreeds(query) {
      return axios({
        method: 'GET',
        url: `${process.env.API_URL}/breeds`,
        params: query,
      });
    },
    getDungeons(query) {
      return axios({
        method: 'GET',
        url: `${process.env.API_URL}/dungeons`,
        params: query,
      });
    },
    getJobs(query) {
      return axios({
        method: 'GET',
        url: `${process.env.API_URL}/jobs`,
        params: query,
      });
    },
    getJob(id, query) {
      return axios({
        method: 'GET',
        url: `${process.env.API_URL}/jobs/${id}`,
        params: query,
      });
    },
    getKamasReward(kamasScaleWithPlayerLevel, optimalLevel, kamasRatio, duration, playerLevel) {
      const lvl = kamasScaleWithPlayerLevel && playerLevel !== -1 ? playerLevel : optimalLevel;
      return Math.floor((lvl ** 2.0 + 20.0 * lvl - 20.0) * kamasRatio * duration);
    },
    getXpReward(playerLevel, optimalLevel, xpRatio, duration) {
      playerLevel = playerLevel === -1 ? 200 : playerLevel;

      if (playerLevel > optimalLevel) {
        const rewardLevel = Math.min(playerLevel, optimalLevel * 1.5);
        /* eslint-disable */
        const fixeOptimalLevelExperienceReward = optimalLevel * ((100.0 + 2.0 * optimalLevel) ** 2) / 20.0 * duration * xpRatio;
        const fixeLevelExperienceReward = rewardLevel * ((100.0 + 2.0 * rewardLevel) ** 2) / 20.0 * duration * xpRatio;
        /* eslint-enable */

        const reducedOptimalExperienceReward = (1.0 - 0.7) * fixeOptimalLevelExperienceReward;
        const reducedExperienceReward = 0.7 * fixeLevelExperienceReward;
        return Math.floor(reducedExperienceReward + reducedOptimalExperienceReward);
      }
      /* eslint-disable */
      return Math.floor(playerLevel * ((100.0 + 2.0 * playerLevel) ** 2.0) / 20.0 * duration * xpRatio);
      /* eslint-enable */
    },
    characteristic_categories_name: ['', '', 'Primaire', 'Secondaire', 'Dommages', 'Résistances'],
    characteristics_id: characteristics,
    characteristics_icon: {
      1: 'stat:pa',
      10: 'stat:force',
      11: 'stat:pv',
      12: 'stat:sagesse',
      13: 'stat:chance',
      14: 'stat:agilite',
      15: 'stat:intelligence',
      16: 'stat:dommages',
      18: 'stat:critique',
      19: 'stat:portee',
      23: 'stat:pm',
      25: 'stat:puissance',
      26: 'stat:invocations',
      27: 'stat:esquive-pa',
      28: 'stat:esquive-pm',
      33: 'stat:resistance-terre',
      34: 'stat:resistance-feu',
      35: 'stat:resistance-eau',
      36: 'stat:resistance-air',
      37: 'stat:resistance-neutre',
      40: 'stat:pods',
      44: 'stat:initiative',
      48: 'stat:prospection',
      49: 'stat:soins',
      50: 'stat:renvoi',
      54: 'stat:resistance-terre',
      55: 'stat:resistance-feu',
      56: 'stat:resistance-eau',
      57: 'stat:resistance-air',
      58: 'stat:resistance-neutre',
      69: 'stat:puissance-pieges',
      70: 'stat:dommages-pieges',
      78: 'stat:fuite',
      79: 'stat:tacle',
      82: 'stat:retrait-pa',
      83: 'stat:retrait-pm',
      84: 'stat:dommages-poussee',
      85: 'stat:resistance-poussee',
      86: 'stat:dommages-critiques',
      87: 'stat:resistance-critiques',
      88: 'stat:dommages-terre',
      89: 'stat:dommages-feu',
      90: 'stat:dommages-eau',
      91: 'stat:dommages-air',
      92: 'stat:dommages-neutre',
      112: 'stat:dommages-melee', // 125 (in search)
      113: 'stat:dommages-distance', // 120 (in search)
      114: 'stat:dommages-armes', // 122 (in search)
      115: 'stat:resistance-melee', // 124 (in search)
      116: 'stat:dommages-sorts', // 123 (in search)
      117: 'stat:resistance-distance', // 121 (in search)
      118: 'stat:resistance-armes', // 142 (in search)
      119: 'stat:resistance-sorts', // 141 (in search)
    },
    characteristics_link_search: {
      125: 112,
      120: 113,
      122: 114,
      124: 115,
      123: 116,
      121: 117,
      142: 118,
      141: 119,
    },
  };

  Vue.prototype.$api = api;
  return api;
};
