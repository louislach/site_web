import createPersistedState from 'vuex-persistedstate';

export default async ({ store }) => {
  // window.setTimeout is needed to ensure than data will be rehydrated in the client-side,
  // i know, that is ugly and don't make sense.
  window.setTimeout(() => {
    createPersistedState({
      key: 'dofusdb-local',
    })(store);
  }, 0);
};
