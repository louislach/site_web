export default {
  NEUTRE: 0,
  TERRE: 1,
  FEU: 2,
  EAU: 3,
  AIR: 4,
  FORCE: 1,
  INTELLIGENCE: 2,
  CHANCE: 3,
  AGILITE: 4,
};
