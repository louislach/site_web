<template>
  <q-dialog
    ref="dialog"
    full-width
    full-height
    @hide="onDialogHide"
  >
    <q-card dark class="q-dialog-plugin">
      <q-bar>
        <div>Selectionner un Serveur</div>

        <q-space />

        <q-btn dense flat icon="close" v-close-popup>
          <q-tooltip content-class="bg-white text-primary">Close</q-tooltip>
        </q-btn>
      </q-bar>

      <q-inner-loading :showing="servers === null" />

      <q-card-section
        v-if="servers"
        class="row justify-center"
      >
        <div
          v-for="(server) of servers"
          :key="server._id"
          class="col-6 col-sm-4 col-md-2 q-pa-xs"
        >
          <server
            :server="server"
            class="text-body1 text-shadow-2 cursor-pointer"
            @click="select(server)"
          />
        </div>
      </q-card-section>
    </q-card>
  </q-dialog>
</template>

<script>
import Server from 'components/Common/Server';

export default {
  components: {
    Server,
  },
  props: {
    // ...your custom props
  },
  computed: {
    servers: {
      get() {
        const res = this.$store.state.server.servers;
        return res ? JSON.parse(JSON.stringify(res)) : res;
      },
      set(value) {
        return this.$store.commit('server/setServers', value);
      },
    },
  },
  async mounted() {
    const nbServer = (await this.$api.getServers({ $limit: 0 })).data.total;
    if (!this.servers || nbServer > this.servers.length) {
      this.servers = [];
      this.fetchServers();
    }
  },
  methods: {
    async fetchServers(query) {
      query = query || {
        $limit: 50,
        '$sort[name]': 1,
      };
      const response = (await this.$api.getServers(query)).data;

      this.servers = this.servers.concat(response.data);

      if (response.total > this.servers.length) {
        query.$skip += 50;
        await this.fetchServers(query);
      }
    },
    select(server) {
      this.$store.commit('server/setSelected', server);
      this.hide();
    },
    show() {
      this.$refs.dialog.show();
    },
    hide() {
      this.$refs.dialog.hide();
    },
    onDialogHide() {
      this.$emit('hide');
    },
    onOKClick() {
      this.$emit('ok');
      this.hide();
    },
    onCancelClick() {
      this.hide();
    },
  },
};
</script>
