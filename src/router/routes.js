import Vue from 'vue';

const routes = [
  {
    path: '/',
    component: () => import('layouts/MainLayout.vue'),
    beforeEnter: (to, from, next) => {
      if (!Vue.prototype.$store.getters['user/isConnected'] && Vue.prototype.$store.state.user.token) {
        Vue.prototype.$store.commit('user/logout');
      }
      next();
    },
    children: [
      {
        path: '',
        name: 'index',
        component: () => import('pages/Index.vue'),
      },
      { path: 'encyclopedia', name: 'encyclopedia', component: () => import('pages/Encyclopedia') },
      {
        path: 'database/objects/type/:id',
        name: 'objectsByType',
        component: () => import('pages/Database/Object/ObjectsByType'),
      },
      {
        path: 'database/object/:id',
        name: 'object',
        component: () => import('pages/Database/Object/Object'),
      },
      {
        path: 'database/quest/:id',
        name: 'quest',
        component: () => import('pages/Database/Quest/Quest'),
      },
      {
        path: 'database/quests/category/:id',
        name: 'questsByCategory',
        component: () => import('pages/Database/Quest/QuestsByCategory'),
      },
      {
        path: 'database/achievements/category/:id',
        name: 'achievementsByCategory',
        component: () => import('pages/Database/Achievement/AchievementsByCategory'),
      },
      {
        path: 'database/achievements/:id',
        name: 'achievement',
        component: () => import('pages/Database/Achievement/Achievement'),
      },
      // Tools
      {
        path: 'tools/treasure-hunt',
        name: 'treasureHunt',
        component: () => import('pages/Tools/TreasureHunt/TreasureHunt'),
      },
      {
        path: 'tools/portals',
        name: 'portals',
        component: () => import('pages/Tools/Portals/Portals'),
      },
      {
        path: 'tools/group-finder',
        name: 'groupFinder',
        component: () => import('pages/Tools/GroupFinder/GroupFinderSearch'),
      },
      {
        path: 'tools/jobs-xp',
        name: 'jobsXP',
        component: () => import('pages/Tools/Jobs/JobsXP'),
      },
      {
        path: 'tools/stuff/creator',
        name: 'stuffCreator',
        component: () => import('pages/Tools/Stuff/StuffCreator'),
      },
      {
        path: 'tools/stuff/editor/:id',
        name: 'stuffEditor',
        component: () => import('pages/Tools/Stuff/StuffCreator'),
      },
      {
        path: 'tools/stuff/copy/:copyFrom',
        name: 'stuffCopy',
        component: () => import('pages/Tools/Stuff/StuffCreator'),
      },
      {
        path: 'tools/stuff/:id',
        name: 'stuff',
        component: () => import('pages/Tools/Stuff/Stuff'),
      },
      {
        path: 'tools/stuffs',
        name: 'stuffs',
        component: () => import('pages/Tools/Stuff/Stuffs'),
      },
      {
        path: 'tools/craft-manager',
        name: 'craftManager',
        component: () => import('pages/Tools/CraftManager/CraftManager'),
      },
      {
        path: 'tools/breeding/crossing',
        name: 'crossingBreeding',
        component: () => import('pages/Tools/Breeding/CrossingBreeding'),
      },
      {
        path: 'tools/map',
        name: 'map',
        component: () => import('pages/Tools/Map/Map'),
      },
      // Sign
      {
        path: 'signin',
        name: 'signin',
        component: () => import('pages/Sign/SignIn'),
      },
      {
        path: 'signup',
        name: 'signup',
        component: () => import('pages/Sign/SignUp'),
      },
      // User
      {
        path: 'my-characters',
        name: 'myCharacters',
        component: () => import('pages/Characters/MyCharacters'),
      },
    ],
  },
];

// Always leave this as last one
if (process.env.MODE !== 'ssr') {
  routes.push({
    path: '*',
    component: () => import('pages/Error404.vue'),
  });
}

export default routes;
