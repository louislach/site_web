export function toggleFinished(state, id) {
  const index = state.finished.indexOf(id);

  if (index !== -1) {
    if (index === 0) {
      state.finished.shift();
    } else {
      state.finished.splice(index, 1);
    }
  } else {
    state.finished.push(id);
  }
}

export function setCategories(state, categories) {
  state.categories = JSON.parse(JSON.stringify(categories));
  state.categories_updated_at = (new Date()).getTime();
}
