export function setControlled(state, value) {
  state.controlled = JSON.parse(JSON.stringify(value));
}
