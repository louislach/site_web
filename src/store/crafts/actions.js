export function addCraft({ state, commit }, item) {
  const crafts = JSON.parse(JSON.stringify(state.crafts));
  const index = crafts.findIndex((e) => e.item.id === item.id);

  if (index === -1) {
    crafts.push(JSON.parse(JSON.stringify({
      item,
      quantity: item.quantity || 1,
      finished: false,
      recipeSearched: false,
    })));
  } else {
    crafts[index].quantity += item.quantity || 1;
  }

  commit('setCrafts', crafts);
}
