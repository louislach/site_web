export function setCrafts(state, value) {
  state.crafts = JSON.parse(JSON.stringify(value));
}

export function setXPCrafts(state, value) {
  state.xp_crafts = JSON.parse(JSON.stringify(value));
}

export function setXPStartingXP(state, value) {
  state.xp_starting_xp = value;
}

export function setXPJob(state, value) {
  state.xp_job = value;
}

export function setXPCoef(state, value) {
  state.xp_coef = value;
}
