export default function () {
  return {
    crafts: [],
    xp_crafts: [],
    xp_job: null,
    xp_starting_xp: 0,
    xp_coef: 100,
  };
}
