import Vue from 'vue';
import Vuex from 'vuex';

import user from './user';
import search from './search';
import server from './server';
import type from './type';
import quest from './quest';
import achievement from './achievement';
import treasureHunt from './treasureHunt';
import map from './map';
import meta from './meta';
import config from './config';
import crafts from './crafts';
import breedings from './breedings';
import admin from './admin';

Vue.use(Vuex);

/*
 * If not building with SSR mode, you can
 * directly export the Store instantiation;
 *
 * The function below can be async too; either use
 * async/await or return a Promise which resolves
 * with the Store instance.
 */

export default function (/* { ssrContext } */) {
  const Store = new Vuex.Store({
    modules: {
      search,
      type,
      quest,
      achievement,
      treasureHunt,
      user,
      server,
      map,
      meta,
      config,
      crafts,
      breedings,
      admin,
    },

    // enable strict mode (adds overhead!)
    // for dev mode only
    strict: process.env.DEV,
  });

  Vue.prototype.$store = Store;
  return Store;
}
