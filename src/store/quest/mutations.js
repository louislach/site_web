export function addOrRemoveToFinishedQuests(state, questId) {
  const index = state.finished_quests.indexOf(questId);

  if (index !== -1) {
    if (index === 0) {
      state.finished_quests.shift();
    } else {
      state.finished_quests = state.finished_quests.splice(index, 1);
    }
  } else {
    state.finished_quests.push(questId);
  }
}

export function setCategories(state, categories) {
  state.categories = JSON.parse(JSON.stringify(categories));
  state.categories_updated_at = (new Date()).getTime();
}
