export function advanced(state, value) {
  state.advanced = value;
}

export function setSearchCharacteristics(state, value) {
  state.search_characteristics = JSON.parse(JSON.stringify(value));
}

export function setSearchTypes(state, value) {
  state.search_types = JSON.parse(JSON.stringify(value));
}

export function setCharacteristics(state, value) {
  state.characteristics = value;
  state.characteristics_updated_at = (new Date()).getTime();
}

export function setName(state, value) {
  state.name = value;
}

export function setLevels(state, value) {
  state.levels = [...value];
}

export function setLoadingCharacteristics(state, value) {
  state.loading_characteristics = value;
}

export function itemDetail(state, value) {
  state.itemDetail = value;
}
