export default function () {
  return {
    advanced: false,
    name: '',
    levels: [0, 200],
    characteristics: [
      {
        id: 1,
        name: 'PA',
        categoryId: 1,
      },
    ],
    characteristics_updated_at: 0,
    loading_characteristics: false,
    search_characteristics: [{
      characteristic: null,
      operator: '>=',
      value: 1,
    }],
    search_types: [],
    itemDetail: true,
  };
}
