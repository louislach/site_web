export function setServers(state, value) {
  state.servers = value ? JSON.parse(JSON.stringify(value)) : value;
}

export function setSelected(state, value) {
  state.selected = value;
}
