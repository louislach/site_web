export function setTypes(state, types) {
  state.types = JSON.parse(JSON.stringify(types));
  state.types_updated_at = (new Date()).getTime();
}
