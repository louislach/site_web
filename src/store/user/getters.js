import jwt from 'jsonwebtoken';

export function hasRole(state, role) {
  if (!state.user || !state.user.roles) {
    return false;
  }

  return state.user.roles.includes(role);
}

export function isConnected(state) {
  if (!state.token || !state.user) {
    return false;
  }

  const decoded = jwt.decode(state.token);
  const now = new Date();
  return (decoded.exp * 1000 > now.getTime());
}
