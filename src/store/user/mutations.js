export function setToken(state, value) {
  state.token = value;
}

export function setUser(state, value) {
  state.user = value;
}

export function logout(state) {
  state.user = null;
  state.token = null;
}

export function setCharacters(state, value) {
  state.characters = JSON.parse(JSON.stringify(value));
}

export function setBreeds(state, value) {
  state.breeds = JSON.parse(JSON.stringify(value));
}
