import elements from 'src/enums/elements';
import characteristics from 'src/enums/characteristics';

function calcul(range, charac, damages, finalDamageBonus) {
  const coef = charac / 100;

  range[0] += Math.round(range[0] * coef) + damages;
  range[1] += Math.round(range[1] * coef) + damages;

  if (finalDamageBonus) {
    range[0] += Math.round((range[0] / 100) * finalDamageBonus);
    range[1] += Math.round((range[1] / 100) * finalDamageBonus);
  }
}

export function calculHeal(range, element, stats) {
  let charac;
  stats = stats || {};
  const soins = stats[characteristics.soins];

  switch (element) {
    case elements.NEUTRE:
      charac = stats[characteristics.force] || 0;
      calcul(range, charac, soins, 0);
      break;
    case elements.TERRE:
      charac = stats[characteristics.force] || 0;
      calcul(range, charac, soins, 0);
      break;
    case elements.FEU:
      charac = stats[characteristics.intelligence] || 0;
      calcul(range, charac, soins, 0);
      break;
    case elements.EAU:
      charac = stats[characteristics.chance] || 0;
      calcul(range, charac, soins, 0);
      break;
    case elements.AIR:
      charac = stats[characteristics.agilite] || 0;
      calcul(range, charac, soins, 0);
      break;
    default:
      break;
  }

  return range;
}

export function calculDamages(range, element, stats, crit, finalDamageBonus) {
  let charac;
  stats = stats || {};
  let damages;

  switch (element) {
    case elements.NEUTRE:
      charac = (stats[characteristics.force] || 0) + (stats[characteristics.puissance] || 0);
      damages = (stats[characteristics['dommages-neutre']] || 0) + (stats[characteristics.dommages] || 0);
      if (crit) {
        damages += stats[characteristics['dommages-critiques']];
      }
      calcul(range, charac, damages, finalDamageBonus);
      break;
    case elements.TERRE:
      charac = (stats[characteristics.force] || 0) + (stats[characteristics.puissance] || 0);
      damages = (stats[characteristics['dommages-terre']] || 0) + (stats[characteristics.dommages] || 0);
      if (crit) {
        damages += stats[characteristics['dommages-critiques']];
      }
      calcul(range, charac, damages, finalDamageBonus);
      break;
    case elements.FEU:
      charac = (stats[characteristics.intelligence] || 0) + (stats[characteristics.puissance] || 0);
      damages = (stats[characteristics['dommages-feu']] || 0) + (stats[characteristics.dommages] || 0);
      if (crit) {
        damages += stats[characteristics['dommages-critiques']];
      }
      calcul(range, charac, damages, finalDamageBonus);
      break;
    case elements.EAU:
      charac = (stats[characteristics.chance] || 0) + (stats[characteristics.puissance] || 0);
      damages = (stats[characteristics['dommages-eau']] || 0) + (stats[characteristics.dommages] || 0);
      if (crit) {
        damages += stats[characteristics['dommages-critiques']];
      }
      calcul(range, charac, damages, finalDamageBonus);
      break;
    case elements.AIR:
      charac = (stats[characteristics.agilite] || 0) + (stats[characteristics.puissance] || 0);
      damages = (stats[characteristics['dommages-air']] || 0) + (stats[characteristics.dommages] || 0);
      if (crit) {
        damages += stats[characteristics['dommages-critiques']];
      }
      calcul(range, charac, damages, finalDamageBonus);
      break;
    default:
      break;
  }

  return range;
}
