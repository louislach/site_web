module.exports = {
  dateAgoString(date, displaySeconds) {
    const now = new Date();
    date = new Date(date);

    const diffSeconds = Math.floor((now.getTime() - date.getTime()) / 1000);
    const seconds = diffSeconds % 60;
    const minutes = Math.floor(diffSeconds / 60) % 60;
    const hours = Math.floor(diffSeconds / 3600);

    let string = 'Il y a';
    if (hours) {
      string += ` ${hours} heure${hours > 1 ? 's' : ''}`;
    }
    if (!displaySeconds || minutes) {
      string += ` ${minutes} minute${minutes > 1 ? 's' : ''}`;
    }
    if (displaySeconds) {
      string += ` ${seconds} seconde${seconds > 1 ? 's' : ''}`;
    }
    return string;
  },
  MetaProperty(properties) {
    const result = [];
    Object.keys(properties).forEach((key) => {
      result.push({
        name: key,
        property: key,
        content: properties[key],
      });
    });

    return result;
  },
  getMeta(metas, key) {
    let item = {};

    metas.forEach((e) => {
      if (e.name === key) {
        item = e;
      }
    });

    return item;
  },
};
