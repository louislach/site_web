function findOptionnalDices(pattern, params) {
  let patternCopyFirstPart = null;
  const patternCopySecondPart = null;
  let nBlancDebut = 0;
  let nBlancFin = 0;
  let returnValue = pattern;
  const posAcc1 = pattern.indexOf('{');
  const posAcc2 = pattern.indexOf('}');

  if (posAcc1 >= 0 && posAcc2 > posAcc1) {
    nBlancDebut = 0;
    while (pattern.charAt(posAcc1 - (nBlancDebut + 1)) === ' ') {
      nBlancDebut += 1;
    }
    nBlancFin = 0;
    while (pattern.charAt(posAcc2 + (nBlancFin + 1)) === ' ') {
      nBlancFin += 1;
    }
    pattern = pattern.substring(0, posAcc1 - (2 + nBlancDebut));
    pattern = pattern.substring(posAcc2 - posAcc1 + 5 + nBlancFin + nBlancDebut,
      pattern.length - (posAcc2 - posAcc1));

    if (pattern.charAt(0) === '#' && pattern.charAt(pattern.length - 2) === '#') {
      if (!params[1] && !params[2] && !params[3]) {
        patternCopyFirstPart += params[0];
      } else if (params[0] === 0 && params[1] === 0) {
        patternCopyFirstPart += params[2];
      } else if (!params[2]) {
        pattern = pattern.substring(0, pattern.indexOf('#')) + params[0] + pattern.substring(
          pattern.indexOf('{') + 5, pattern.indexOf('}'),
        ) + params[1] + pattern.substring(
          pattern.indexOf('#', pattern.indexOf('}')),
        );
        patternCopyFirstPart += pattern;
      } else {
        pattern = pattern.substring(0, pattern.indexOf('#')) + (params[0] + params[2])
          + pattern.substring(pattern.indexOf('{') + 5, pattern.indexOf('}')) + (params[0]
            * params[1] + params[2]) + pattern.substring(pattern.indexOf('#', pattern.indexOf('}')));
        patternCopyFirstPart += pattern;
      }
      returnValue = patternCopyFirstPart + patternCopySecondPart;
    }
  }
  return returnValue;
}

export function decodeDescription(pattern, params) {
  let nextSharp = 0;
  let nextTilde = 0;
  let nextBrace = 0;
  let nextBracket = 0;
  let n = NaN;
  let oldLength = NaN;
  let n1 = NaN;
  let pos = 0;
  let rstr = null;
  let pos2 = 0;
  let n2 = NaN;
  pattern = findOptionnalDices(pattern, params);
  let actualIndex = 0;

  if (pattern.indexOf('[UNKNOWN') === 0) {
    return '';
  }

  // eslint-disable-next-line
  while (true) {
    nextSharp = pattern.indexOf('#', actualIndex);
    nextTilde = pattern.indexOf('~', actualIndex);
    nextBrace = pattern.indexOf('{', actualIndex);
    nextBracket = pattern.indexOf('[', actualIndex);

    if (nextSharp !== -1 && (nextTilde === -1 || nextSharp < nextTilde)
      && (nextBrace === -1 || nextSharp < nextBrace)
      && (nextBracket === -1 || nextSharp < nextBracket)) {
      n = parseInt(pattern.charAt(nextSharp + 1), 10);
      oldLength = pattern.length;

      if (!Number.isNaN(n)) {
        if (params[n - 1] || (params[n - 1] === 0 && n === 1)) {
          pattern = pattern.substring(0, nextSharp) + params[n - 1]
            + pattern.substring(nextSharp + 2);
        } else {
          pattern = pattern.substring(0, nextSharp) + pattern.substring(nextSharp + 2);
        }
      }
      actualIndex = nextSharp + pattern.length - oldLength;
    } else if (nextTilde !== -1 && (nextBrace === -1 || nextTilde < nextBrace)
      && (nextBracket === -1 || nextTilde < nextBracket)) {
      n1 = parseInt(pattern.charAt(nextTilde + 1), 10);

      if (!Number.isNaN(n1)) {
        if (params[n1 - 1] || (params[n1 - 1] === 0 && n1 === 1)) {
          pattern = pattern.substring(0, nextTilde) + pattern.substring(nextTilde + 2);
        } else {
          break;
        }
      }
      actualIndex = nextTilde;
    } else if (nextBrace !== -1 && (nextBracket === -1 || nextBrace < nextBracket)) {
      pos = pattern.indexOf('}', nextBrace);
      rstr = decodeDescription(pattern.substring(nextBrace + 1, pos), params);
      pattern = pattern.substring(0, nextBrace) + rstr + pattern.substring(pos + 1);
      actualIndex = nextBrace;
    } else if (nextBracket !== -1) {
      pos2 = pattern.indexOf(']', nextBracket);
      n2 = Number(pattern.substring(nextBracket + 1, pos2));

      if (!Number.isNaN(n2)) {
        pattern = `${pattern.substring(0, nextBracket) + params[n2]} ${pattern.substring(pos2 + 1)}`;
      }
      actualIndex = nextBracket;
    }

    if (!(nextSharp !== -1 || nextTilde !== -1 || nextBrace !== -1 || nextBracket !== -1)) {
      return pattern;
    }
  }
  return pattern.substring(0, nextTilde);
}

export function decodeCombine(pattern, params) {
  let nextTilde = 0;
  let nextBrace = 0;
  let key = null;
  let pos = 0;
  let content = null;
  let twoDotsPos = 0;
  let rstr = null;
  let actualIndex = 0;

  // eslint-disable-next-line
  while (true) {
    nextTilde = pattern.indexOf('~', actualIndex);
    nextBrace = pattern.indexOf('{', actualIndex);

    // next operator is Tilde
    if (nextTilde !== -1 && (nextBrace === -1 || nextTilde < nextBrace)) {
      key = pattern.charAt(nextTilde + 1);
      if (params[key]) {
        pattern = pattern.substring(0, nextTilde) + pattern.substring(nextTilde + 2);
        actualIndex = nextTilde;
      } else {
        break;
      }
      // next operator is Brace
    } else if (nextBrace !== -1) {
      pos = pattern.indexOf('}', nextBrace);
      content = pattern.substring(nextBrace + 1, pos);
      twoDotsPos = -1;

      if (pos > -1) {
        twoDotsPos = content.indexOf(':');
      }

      if ((twoDotsPos === -1 || twoDotsPos + 1 > content.length
        || content.charAt(twoDotsPos + 1) !== ':') && content.indexOf('~') !== -1) {
        rstr = decodeCombine(content, params);
        if (content !== rstr) {
          pattern = pattern.substring(0, nextBrace) + rstr + pattern.substring(pos + 1);
        }
        actualIndex = nextBrace;
      } else {
        actualIndex = pos;
      }

      if (nextBrace === -1 && nextTilde === -1) {
        return pattern;
      }
    }
    return pattern.substring(0, nextTilde);
  }
  return '';
}
